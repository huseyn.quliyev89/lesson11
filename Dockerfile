FROM openjdk:17
COPY build/libs/*.jar /app/
WORKDIR /app/
RUN mv /app/*.jar /app/one-to-many.jar
ENTRYPOINT ["java"]
CMD ["-jar","/app/one-to-many.jar"]

package az.ingress.repository;

import az.ingress.model.Bank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BankRepository extends JpaRepository<Bank,String> {

    //@Query(nativeQuery = true, value = "select count(b.bank_code) FROM Bank b")
    @Query(value = "select count(b.bankCode) FROM Bank b")
    public Integer countBanksByBankCode();

}

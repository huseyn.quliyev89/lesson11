package az.ingress.mapper;

import az.ingress.dto.BankDto;
import az.ingress.dto.BranchDto;
import az.ingress.model.Branch;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BranchMapper {
    BranchDto mapToBranchDto(Branch branch);
    Branch mapToBranch(BranchDto branchDto);

    List<BranchDto> mapToBranchDtoList(List<Branch> branchList);

    List<Branch> mapToBranchList(List<BranchDto> branchDtoList);



}

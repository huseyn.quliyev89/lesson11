package az.ingress.service;

import az.ingress.dto.TotalBranchByBankDto;
import az.ingress.model.Branch;
import az.ingress.repository.BranchRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BranchServiceImpl implements BranchService {
    private final BranchRepository branchRepository;

    public BranchServiceImpl(BranchRepository branchRepository) {
        this.branchRepository = branchRepository;
    }

    public Branch getBranchByBranchCode(String branchCode) {
        Optional<Branch>  branch =   branchRepository.findById(branchCode);

        if (branch.isPresent()) {
            return branch.get();
        } else {
            throw new RuntimeException("Record not found with branchCode : " + branchCode);
        }
    }


    @Override
    public List<Branch> getBranches() {
        return branchRepository.findAll();
    }

    @Override
    public Branch createBranch(Branch branch) {
        return branchRepository.save(branch);
    }

    @Override
    public Branch updateBranch(String branchCode, Branch branch) {
        Optional<Branch> branchDb = branchRepository.findById(branch.getBranchCode());

        if (branchDb.isPresent()) {
            Branch branchUpdate = branchDb.get();
            branchUpdate.setBranchCode(branch.getBranchCode());
            branchUpdate.setBranchName(branch.getBranchName());
            branchUpdate.setBranchOpeningDate(branch.getBranchOpeningDate());

            return branchRepository.save(branchUpdate);
        } else {
            throw new RuntimeException("Record not found with branchCode : " + branchCode);
        }
    }
    @Override
    public void deleteBranch(String branchCode) {
        branchRepository.deleteById(branchCode);
    }



    @Override
    public List<TotalBranchByBankDto> totalBranchByBank() {
        return branchRepository.totalBranchByBank();
    }
}

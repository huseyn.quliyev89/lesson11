package az.ingress.dto;

import az.ingress.model.Bank;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BranchDto {
    String branchCode;
    String branchName;
    LocalDate branchOpeningDate;
    @JsonIgnoreProperties("branches")
    //@ToString.Exclude
    Bank bank;
}

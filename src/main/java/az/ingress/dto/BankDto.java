package az.ingress.dto;

import az.ingress.model.Branch;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BankDto {
    String bankCode;
    String bankName;
    String bankSwiftCode;
    LocalDate bankOpeningDate;
    @JsonIgnoreProperties("bank")
    List<Branch> branches;

}
